module gitlab.com/inbioz/d

go 1.16

require (
	gorm.io/driver/mysql v1.2.3
	gorm.io/gorm v1.22.5
)
