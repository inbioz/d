package d

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// D database/sql
type D struct {
	DB *gorm.DB
}

// Open wrapper for sql.Open
func Open(conn string) D {

	db, err := gorm.Open(mysql.Open(conn))
	if err != nil {
		panic("Failed to connect to database!")
	}

	return D{DB: db}
}
